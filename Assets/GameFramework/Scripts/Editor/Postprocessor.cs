using UnityEditor;
class Postprocessor : AssetPostprocessor
{
    void OnPreprocessModel()
    {
        var modelImporter = assetImporter as ModelImporter;
        if (modelImporter) modelImporter.globalScale = 1f;
    }
    void OnPreprocessAudio()
    {
        var audioImporter = assetImporter as AudioImporter;
        if (audioImporter)
        {
            audioImporter.threeD = EditorUtility.DisplayDialog("Sound Import", "Import as 2D or 3D sound?", "3D", "2D");
        }
    }

}
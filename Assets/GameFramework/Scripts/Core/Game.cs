﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;


public class Game
{
    public static string GameName = "";
    public static int GameId = 0;
    public static string GameRev = "";

    public Game(string gameName, int gameId, string gameRev)
    {
        GameName = gameName;
        GameId = gameId;
        GameRev = gameRev;
    }
}


namespace GameFramework
{
    using UnityEngine;

    public class GF_Gamecore
    {
        public static int PlayerMaxLevel = 100;

        public static float Gravity = 20.0F;



        private static UnitDifficultyColors _getConColor = UnitDifficultyColors.Skull;

        public static UnitDifficultyColors GetConColor(int playerlvl, int moblvl)
        {
            if (playerlvl + 5 <= moblvl)
            {
                _getConColor = (playerlvl + 10 <= moblvl) ? UnitDifficultyColors.Skull : UnitDifficultyColors.Red;
            }
            else
            {
                switch (moblvl - playerlvl)
                {
                    case 4:
                    case 3:
                        _getConColor = UnitDifficultyColors.Orange;
                        break;

                    case 2:
                    case 1:
                    case 0:
                    case -1:
                    case -2:
                        _getConColor = UnitDifficultyColors.Yellow;
                        break;

                    default:

                        // More adv formula for grey/green lvls:
                        if (playerlvl <= 5)
                        {
                            _getConColor = UnitDifficultyColors.Green; //All others are green.
                        }
                        else
                        {
                            if (playerlvl <= 39)
                            {
                                _getConColor = moblvl <= (playerlvl - 5 - Mathf.Floor(playerlvl / 10)) ? UnitDifficultyColors.Grey : UnitDifficultyColors.Green;
                            }
                            else
                            {
                                //_player over lvl 39:
                                _getConColor = moblvl <= (playerlvl - 1 - Mathf.Floor(playerlvl / 5)) ? UnitDifficultyColors.Grey : UnitDifficultyColors.Green;
                            }
                        }
                        break;
                }
            }
            return _getConColor;
        }

    }
}

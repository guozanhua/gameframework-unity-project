using System;
using UnityEngine;

namespace GameFramework
{
    public class GF_Exp
    {
        private static int _tempExperience = 0;

        public static void AddExperiencePoints(GF_Unit player, GF_Unit target)
        {
            if (player.UnitAttribute.Level >= GF_Gamecore.PlayerMaxLevel) return;

            var tempExp = player.UnitAttribute.ExperiencePoints + GetSingleUnitXpWithRestedBonus(player.UnitAttribute.Level, target.UnitAttribute.Level, 0);

            if (tempExp > GetCurXpCap(player.UnitAttribute.Level))
            {
                for (var i = player.UnitAttribute.Level; i < GF_Gamecore.PlayerMaxLevel; i++)
                {
                    if (tempExp <= GetCurXpCap(player.UnitAttribute.Level))
                        break;
                    tempExp -= GetCurXpCap(player.UnitAttribute.Level);
                    LEVEL_UP(player, 1);
                }
            }
            Debug.LogWarning(tempExp);
            AddExperiencePoints(player, tempExp);
        }

        public static void AddExperiencePoints(GF_Unit unit, int exp)
        {
            unit.UnitAttribute.SetExperiencePointsCap(exp);
        }

        public static int GetBossXp(int playerLevel, int unitLevel)
        {
            return playerLevel < GF_Gamecore.PlayerMaxLevel ? GetUnitXp(playerLevel, unitLevel) * 4 : 0;
        }

        public static int GetEliteUnitXp(int playerLevel, int unitLevel)
        {
            return playerLevel < GF_Gamecore.PlayerMaxLevel ? GetUnitXp(playerLevel, unitLevel) * 2 : 0;
        }

        public static int GetMiniBossXp(int playerLevel, int unitLevel)
        {
            return playerLevel < GF_Gamecore.PlayerMaxLevel ? GetUnitXp(playerLevel, unitLevel) * 3 : 0;
        }

        public static int GetRaidBossXp(int playerLevel, int unitLevel)
        {
            return playerLevel < GF_Gamecore.PlayerMaxLevel ? GetUnitXp(playerLevel, unitLevel) * 5 : 0;
        }

        public static int GetSingleUnitXpWithRestedBonus(int playerLevel, int unitLevel, int rest)
        {
            _tempExperience = GetUnitXp(playerLevel, unitLevel);
            return rest == 0 ? _tempExperience : (rest >= _tempExperience ? _tempExperience * 2 : rest + (_tempExperience - (rest / 2)));
        }

        public static int GetUnitXp(int playerLevel, int unitLevel)
        {
            if (unitLevel >= playerLevel)
            {
                var temp = (int)Math.Round((playerLevel * 5) * (1 + (0.05f * (unitLevel - playerLevel))));
                var tempcap = (int)(playerLevel * 5 * 1.2f);
                if (temp > tempcap)
                {
                    Debug.LogWarning("AddExperiencePointsToHero : " + tempcap);
                    return tempcap;
                }
                Debug.LogWarning("AddExperiencePointsToHero : " + temp);
                return temp;
            }
            switch (GF_Gamecore.GetConColor(playerLevel, unitLevel))
            {
                case 0:
                    Debug.LogWarning("AddExperiencePointsToHero : 0");
                    return 0;
                default:
                    {
                        var r = (playerLevel * 5) * (1 - (playerLevel - unitLevel) / GetZd(playerLevel));
                        Debug.LogWarning("AddExperiencePointsToHero " + r);
                        return r;
                    }
            }
        }

        public static int GetWorldBossXp(int playerLevel, int unitLevel)
        {
            return playerLevel < GF_Gamecore.PlayerMaxLevel ? GetUnitXp(playerLevel, unitLevel) * 6 : 0;
        }

        public static int GetZd(int level)
        {
            if (level <= 7)
            {
                return 5;
            }
            if (level <= 9)
            {
                return 6;
            }
            if (level <= 11)
            {
                return 7;
            }
            if (level <= 15)
            {
                return 8;
            }
            if (level <= 19)
            {
                return 9;
            }
            if (level <= 29)
            {
                return 11;
            }
            if (level <= 39)
            {
                return 12;
            }
            if (level <= 44)
            {
                return 13;
            }
            if (level <= 49)
            {
                return 14;
            }
            if (level <= 54)
            {
                return 15;
            }
            return level <= 59 ? 16 : 17;
        }

        public static void LEVEL_UP(GF_Unit unit, int addLevel)
        {
            if (!unit) throw new ArgumentNullException("unit");
            if (unit.UnitAttribute.Level >= GF_Gamecore.PlayerMaxLevel) return;
            unit.UnitAttribute.Level += addLevel;
            Debug.Log("LEVEL UP! current Level: " + unit.UnitAttribute.Level + " / " + GF_Gamecore.PlayerMaxLevel);
            unit.UnitAttribute.SetExperiencePointsCap(GetCurXpCap(unit.UnitAttribute.Level));
        }

        #region GetCurXpCap


        private const float LevelFactor1 = 50;
        private const float LevelFactor2 = 150;
        private const float LevelFactor3 = 400f;

        /// <summary>
        /// returns the amount of experience points need to level up!!!
        /// </summary>
        /// <param _itemHardoint="_level"></param>
        /// <param name="unitLevel"></param>
        /// <returns></returns>
        public static int GetCurXpCap(int unitLevel)
        {
            return
                (int)Math.Round((LevelFactor1 * Math.Pow(unitLevel, 3) - LevelFactor2 * Math.Pow(unitLevel, 2) + LevelFactor3 * (unitLevel)) / 3);
        }


        #endregion GetCurXpCap
    }
}

    // 
    //private const float LevelFactor1 = 48;
    //private const float LevelFactor2 = 352;
    //private const float LevelFactor3 = 40.8f;
    //private const float LevelFactor4 = 396f;
    //private const float LevelFactor5 = 0.42f;
    //if (unitLevel <= 10)
    //    {
    //        // <= 10
    //        //For all levels lower then 11 the XP to Level can be expressed as the second grade function:
    //        //XP to Level = 40x2 + 360x
    //        return (int)((LevelFactor1 * (Mathf.Pow(unitLevel, 2))) + (LevelFactor2 * unitLevel));
    //    }
    //    else if (unitLevel > 10 && unitLevel <= 27)
    //    {
    //        // 11 - 27
    //        //XP to Level = -.4x3 + 40.4x2 + 396x
    //        return
    //            (int)
    //            ((-1f * (Mathf.Pow(LevelFactor5, 3))) + (Mathf.Pow(LevelFactor3, 2)) +
    //             (LevelFactor4 * unitLevel));
    //    }
    //    else if (unitLevel > 27 && unitLevel <= 59)
    //    {
    //        // 27 - 59
    //        //(65x2 - 165x - 6750) � .82
    //        return (int)((65 * (unitLevel * 2) - (165 * unitLevel) - 6750) * .82f);
    //    }
    //    else if (unitLevel == 60)
    //    {
    //        // 60
    //        //XP to Level = 155 + MXP(CL) x (1344 - 69 - ((69-CL)*(3+(69-CL)*4)))
    //        return (int)(155 + unitLevel * (1344 - 69 - ((69 - unitLevel) * (3 + (69 - unitLevel) * 4))));
    //    }
    //    else
    //    {
    //        // > 60
    //        //XP to Level = 155 + MXP(CL) x (1344 - ((69-CL)*(3+(69-CL)*4)))
    //        return (int)(155 + (unitLevel) * (1344 - ((69 - unitLevel) * (3 + (69 - unitLevel) * 4))));
    //    }

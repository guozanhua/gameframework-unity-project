﻿using UnityEngine;
using System.Collections;

public class TestQuests
{

    #region local Missions for TESTING GUI!!!!
    public static string[] QuestLogLocal = 
    { //id|curMissionObjective|started_at|state(completed...false or true)
        "1|MissionObjective1-1*MissionObjective2-1|2",
        "2|MissionObjective1-1*MissionObjective2-1|2"
    };

    public static string[] _MasterMissionArray = 
    { 
        ///0 id|1 MissionLevel|2 nextMissionIDinMissionChain|3 requiredMissionID|4 MissionAvailabilityType|5 MissionGiverNameStart|6 MissionGiverNameEnd|7 MissionName|8 MissionDescription|9 MissionSummary|10 completionText|11 MissionObjectives|12 expReward|13 rewardItems|14 money|15 FactionReputation|16 MissionZone
        "1|1|2|0|0|General McCiller|General McCiller|MissionName1|MissionDescription|MissionSummary|completionText|MissionObjective1-5|MissionObjective2-5|10|rewardItem1-rewardItem2|1|0-100|0|1",
        "2|1|3|1|0|General McCiller|General McCiller|MissionName1|MissionDescription|MissionSummary|completionText|MissionObjective1-3|MissionObjective2-3|10|rewardItem1-rewardItem2|1|0-100|0|1",
        "3|1|4|2|0|General McCiller|General McCiller|MissionName1|MissionDescription|MissionSummary|completionText|MissionObjective1-10|MissionObjective2-1|10|rewardItem1-rewardItem2|1|0-100|0|1",
        "4|1|5|3|0|General McCiller|General McCiller|MissionName1|MissionDescription|MissionSummary|completionText|MissionObjective1-10|MissionObjective2-1|10|rewardItem1-rewardItem2|1|0-100|0|1",
        "5|1|14|4|0|General McCiller|General McCiller|MissionName1|MissionDescription|MissionSummary|completionText|MissionObjective1-10|MissionObjective2-1|10|rewardItem1-rewardItem2|1|0-100|0|1",
        "14|1|7|0|0|General McCiller|General McCiller|MissionName8|MissionDescription|MissionSummary|completionText|MissionObjective1-10|MissionObjective2-1|10|rewardItem1-rewardItem2|1|0-100|0|1",
        "7|1|8|14|0|General McCiller|General McCiller|MissionName2|MissionDescription|MissionSummary|completionText|MissionObjective1-10|MissionObjective2-1|10|rewardItem1-rewardItem2|1|0-100|0|1",
        "8|2|0|7|0|General McCiller|General McCiller|MissionName2|MissionDescription|MissionSummary|completionText|MissionObjective1-10|MissionObjective2-1|10|rewardItem1-rewardItem2|1|0-100|0|1",
        "9|1|0|0|0|General McCiller|General McCiller|MissionName3|MissionDescription|MissionSummary|completionText|MissionObjective1-10|MissionObjective2-1|10|rewardItem1-rewardItem2|1|0-100|0|1",
        "10|1|11|0|0|General McCiller|General McCiller|MissionName4|MissionDescription|MissionSummary|completionText|MissionObjective1-10|MissionObjective2-1|10|rewardItem1-rewardItem2|1|0-100|0|1",
        "11|1|12|10|0|General McCiller|General McCiller|MissionName4|MissionDescription|MissionSummary|completionText|MissionObjective1-10|MissionObjective2-1|10|rewardItem1-rewardItem2|1|0-100|0|1",
        "12|1|0|11|0|General McCiller|General McCiller|MissionName4|MissionDescription|MissionSummary|completionText|MissionObjective1-10|MissionObjective2-1|10|rewardItem1-rewardItem2|1|0-100|0|1",
        "13|1|0|0|0|General McCiller|General McCiller|MissionName4|MissionDescription|MissionSummary|completionText|MissionObjective1-10|MissionObjective2-1|10|rewardItem1-rewardItem2|1|0-100|0|1"
    };
    #endregion
}
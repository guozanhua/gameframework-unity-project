﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;

namespace GameFramework
{
    public enum DataBaseStages
    {
        Development,
        MySqlLocal,
        MySqlOnline,
        SqLite //TODO
    }

    public class GF_QuestManager
    {
        //public Dictionary<int, GF_Quest> GameQuests = new Dictionary<int, GF_Quest>();


        #region VARS

        private const int MaxQuestsIQuestLog = 20; // max missions 
        public int QuestCount { get; private set; }
        public bool UseScripableObjects = true;
        public readonly Dictionary<int, GF_Quest> MasterQuestDict = new Dictionary<int, GF_Quest>();
        public Dictionary<int, GF_Quest> QuestLog = new Dictionary<int, GF_Quest>();
        public List<int> NpcQuestsToDisplay = new List<int>();
        public DataBaseStages ProjectStage = DataBaseStages.Development;
        public bool MissionsLoaded = false;

        //public QuestManager()
        //{
        //    QuestCount = 0;
        //}

        #endregion VARS

        public GF_Quest NewQuestLogQuest(string questName)
        {
            //Debug.Log(_MissionName);
            return new GF_Quest();
                //Instantiate(Resources.Load("GF_Quest/" + questName, typeof(GF_Quest))) as GF_Quest;

            //return ScriptableObject.CreateInstance<GF_Quest>();
        }

        public void LoadQuestsFromScripable()
        {
            var quest = new GF_Quest[] {};
                // = Resources.LoadAll("GF_Quest", typeof(GF_Quest));
            foreach (var _m in quest)
            {
                //AddToQuestMaster(quest);
                AddToQuestMaster(NewQuestLogQuest(_m.Name));
                //Debug.LogWarning(this.GetType().FullName + " : " + quest.GiverName_Start);

                // TODO
                ///GameObject.Find(_m.GiverNameStart).GetComponent<NPC>().NpcQuestIds.Add(_m.ID);

                //Debug.Log("HASHCODE" + MASTER_Quest_DICT[quest.ID].ID + " " + MASTER_Quest_DICT[quest.ID].GetHashCode());
            }
        }

        #region GetQuestID

        private int GetQuestID(string questString)
        {
            string[] questSplit = questString.Split('|');
            Debug.Log(GetType().FullName + " : " + questSplit[0]);
            return int.Parse(questSplit[0]);
        }

        #endregion GetQuestID

        #region Start

        private void Start()
        {
            Debug.LogWarning("ZUM LADEN DER QUESTS HIER StartCoroutine");
            //StartCoroutine("InitLoadQuests");

            foreach (KeyValuePair<int, GF_Quest> _ms in MasterQuestDict)
            {
                Debug.Log(_ms.Value.Name);
            }
        }

        #endregion Start

        #region InitLoadQuests

        public IEnumerator InitLoadQuests()
        {
            //foreach (KeyValuePair<int, GF_Quest> quest in MASTER_Quest_DICT)
            //{
            //    Debug.Log(quest.Value.RequiredMissionID);
            //    StartCoroutine(GF_GameManager.missionSystemAPI.CREATE_NEW_Mission(quest.Value));
            //}
            //StartCoroutine(GF_GameManager.missionSystemAPI.LOAD_ALL_MissionS());

            //while (!GF_GameManager.missionSystemAPI.MissionsLoaded) yield return 0;

            //StartCoroutine(GF_GameManager.missionSystemAPI.LOAD_MY_MissionLOG(GF_GameManager.PLAYER.Name));
            //while (!GF_GameManager.missionSystemAPI.MissionLogLoaded) yield return 0;

            yield return new WaitForEndOfFrame(); // wait for loading Missions

            //TestMissionsList = new MissionManagerBase();
            switch (ProjectStage)
            {
                    #region DEVELOPMENT

                case DataBaseStages.Development:

                    //if (UseScripableObjects)
                    //{
                    //    LoadQuestsFromScripable();
                    //    foreach (var questString in TestQuests.QuestLogLocal)
                    //    {
                    //        var id = GetQuestID(questString);
                    //        AddQuest(id);
                    //        Debug.Log(GetType().FullName + " : " + questString);
                    //        Debug.Log(GetType().FullName + " : " + "QuestLogLocal " + QuestLog[id].ID + " " +
                    //                  (int) QuestLog[id].State);
                    //    }
                    //}
                    //else 
                    //{
                    foreach (string questString in TestQuests._MasterMissionArray)
                    {
                        int _id = GetQuestID(questString);
                        GF_Quest quest = new GF_Quest();

                        AddToQuestMaster(quest);
                        Debug.Log(this.GetType().FullName + " : " + questString);
                        Debug.Log(this.GetType().FullName + " : " + "QuestLogLocal " + QuestLog[_id].ID + " " + (int)QuestLog[_id].State);
                    }
                    //}
                    MissionsLoaded = true;

                    break;

                    #endregion DEVELOPMENT

                case DataBaseStages.MySqlLocal:
                case DataBaseStages.MySqlOnline:

                    #region LIVE

                    if (UseScripableObjects)
                    {
                        LoadQuestsFromScripable();
                        //foreach (string questString in GF_GameManager.missionSystemAPI._Missionlog)
                        //{
                        //    if (questString.Length > 1)
                        //    {
                        //        int _id = GetQuestID(questString);
                        //        AddQuest(_id);
                        //        Debug.Log(this.GetType().FullName + " : " + questString);
                        //        Debug.Log(this.GetType().FullName + " : " + "QuestLogLocal " + Quest_LOG[_id].ID + " " + (int)Quest_LOG[_id].State);
                        //    }
                        //}
                    }

                    MissionsLoaded = true;
                    break;

                    #endregion LIVE
            }
        }

        #endregion InitLoadQuests

        #region SaveQuestProgress

        public void SaveQuestProgress(GF_Quest quest)
        {
            //switch (projectStage)
            //{
            //    case DataBaseStages.MySQL_Local:
            //    case DataBaseStages.MySQL_Online:
            //        StartCoroutine(GF_GameManager.missionSystemAPI.SAVE_MY_Mission(MainManager.MY_HERO_SCRIPT._CHARACTER_NAME, MainManager.MY_HERO_SCRIPT._CHARACTER_ID, quest));
            //        break;
            //}
        }

        #endregion SaveQuestProgress

        #region AcceptQuestnByID

        public void AcceptQuestnByID(int questID)
        {
            // ist die missison ueberhaupt verfuegbar ?
            if (MasterQuestDict[questID].State == QuestStates.Unavailable)
            {
                Debug.Log(GetType().FullName + " : " + "GF_Quest id " + questID + " not available.");
                return;
            }
            if (QuestLog.Count < MaxQuestsIQuestLog)
            {
                var quest = NewQuestLogQuest(MasterQuestDict[questID].Name);
                quest.State = QuestStates.Accepted;
                MasterQuestDict[quest.ID].State = QuestStates.Accepted;
                SaveQuestProgress(quest);
                AddQuest(quest);
            }
            else
            {
                Debug.Log(GetType().FullName + " : " + "Missionlog full");
            }
        }

        #endregion AcceptQuestnByID

        #region CompleteMission

        public void CompleteMission(int questID)
        {
            Debug.Log(GetType().FullName + " : " +
                      "GF_Quest complete! - you should go to the GF_Quest giver");

            //AudioSource.PlayClipAtPoint(GainedSound, Camera.main.transform.position);
            //showSplash = true;
            //achievementInterface.SplashAchievement = achievement;
            //achievementInterface.showSplashTimer -= achievementInterface.splashCoolDown;

            // count GF_Quest -->  156/1645 Missions  --> achievement

            //main.playerMetrics.CompletedMissionsCount++;
        }

        #endregion CompleteMission

        #region SubmitMission

        public void SubmitMission(GF_Unit unit, int questID)
        {
            AddToFinishedQuest(questID);
            RemovedQuestFromQuestLog(questID);
            AddExp(unit, MasterQuestDict[questID].Level, MasterQuestDict[questID].RewardExperience);
            QuestCount++;
        }

        #endregion SubmitMission

        #region DeclineQuest

        public void DeclineMission(int questID)
        {

        }

        #endregion DeclineQuest

        #region DropQuest

        public void DropQuest(int questID)
        {
            RemovedQuestFromQuestLog(questID);
            //switch (projectStage)
            //{
            //    case DataBaseStages.MySQL_Local:
            //    case DataBaseStages.MySQL_Online:
            //        StartCoroutine(GF_GameManager.missionSystemAPI.DROP_MY_Mission(MainManager.MY_HERO_SCRIPT._CHARACTER_NAME, MainManager.MY_HERO_SCRIPT._CHARACTER_ID, questID));
            //        break;
            //}
        }

        #endregion DropQuest

        #region RemoveQuestItem

        public void RemoveQuestItem(int itemID)
        {
            Debug.Log("removed Item id " + itemID);
        }

        #endregion RemoveQuestItem

        #region AddQuest

        public void AddQuest(int questID)
        {
            try
            {
                var quest = NewQuestLogQuest(MasterQuestDict[questID].Name);
                quest.State = QuestStates.Accepted;
                if (quest.Progress1 < quest.Requirement1Count && quest.Progress2 < quest.Requirement2Count)
                {
                    quest.State = QuestStates.Accepted;
                }
                else
                {
                    quest.State = QuestStates.Completed;
                }
                QuestLog.Add(questID, quest);
                Debug.Log(questID + " Accepted!");
            }
            catch (System.Exception e)
            {
                Debug.Log("+++ questName setzen");
                Debug.LogWarning(e);
            }
        }

        public void AddQuest(GF_Quest quest)
        {
            //quest.State = QuestStates.Accepted;
            QuestLog.Add(quest.ID, quest);
            Debug.Log(quest.ID + " Accepted!");
        }

        #endregion AddQuest

        public void AddToQuestMaster(GF_Quest quest)
        {
            MasterQuestDict.Add(quest.ID, quest);
        }

        #region RemovedQuestFromQuestLog

        public void RemovedQuestFromQuestLog(int questID)
        {
            QuestLog.Remove(questID);
            RemoveQuestItem(123456);
        }

        #endregion RemovedQuestFromQuestLog

        #region AddToFinishedQuest

        public void AddToFinishedQuest(int questID)
        {
            MasterQuestDict[questID].State = QuestStates.Done;

            // upload
        }

        #endregion AddToFinishedQuest

        /// <summary>
        /// Multiplayer - share missions
        /// </summary>
        /// <param name="questID"></param>

        #region ShareQuest

        public void ShareQuest(int questID)
        {
            // to each party member
        }

        #endregion ShareQuest

        #region ValidateQuest

        public void ValidateQuest(int questID)
        {
        }

        #endregion ValidateQuest

        #region CheckQuestGiverForNextQuest

        internal void CheckQuestGiverForNextQuest(GF_NPC npc)
        {
            if (GF_GameManager.Player.target.GetComponent<GF_NPC>() != npc)
            {
                return;
            }

            NpcQuestsToDisplay.Clear();

            foreach (var questID in npc.NpcQuestIds)
            {
                // wenn das level des Spielers zu niedrig ist
                if (GF_GameManager.Player.UnitAttribute.Level < MasterQuestDict[questID].RequiredLevel)
                {
                    Debug.Log(this.GetType().FullName + " : " + "level to low " + questID);
                    MasterQuestDict[questID].State = QuestStates.Unavailable;
                    npc.QuestDialogState = QuestDialogStates.HasNoQuest;
                    continue;
                }

                if (GF_GameManager.Player.UnitAttribute.Level >= MasterQuestDict[questID].RequiredLevel
                    && MasterQuestDict[questID].State == QuestStates.Unavailable)
                {
                    MasterQuestDict[questID].State = QuestStates.Available;
                    NpcQuestsToDisplay.Add(questID);
                    npc.QuestDialogState = QuestDialogStates.HasQuest;
                    continue;
                }

                // hat spieler die mission bereits im log?
                if (QuestLog.ContainsKey(questID))
                {
                    Debug.Log(this.GetType().FullName + " : " + "GF_Quest in Missionlog: id " + questID);

                    //MASTER_Quest_DICT[questID].State = QuestStates.Accepted;
                    NpcQuestsToDisplay.Add(questID);
                    npc.QuestDialogState = QuestDialogStates.HasNoQuest;
                    continue;
                }

                // hat spieler die mission bereits erledigt ?
                if (MasterQuestDict[questID].State == QuestStates.Done)
                {
                    Debug.Log(this.GetType().FullName + " : " + "GF_Quest id " + questID + " done.");

                    //MASTER_Quest_DICT[questID].State = QuestStates.Unavailable;
                    NpcQuestsToDisplay.Add(questID);
                    npc.QuestDialogState = QuestDialogStates.HasNoQuest;
                    continue;
                }

                // AB HIER SIND ALLE MISSIONEN SIND VERFUEGBAR!
                npc.QuestDialogState = QuestDialogStates.HasQuest;
                NpcQuestsToDisplay.Add(questID);

                // mission hat einen vorgaenger? 0 bedeutet: keine vorgaenger mission
                if (MasterQuestDict[questID].RequiredMissionID != 0)
                {
                    // status des vorgaengers abrufen
                    switch (MasterQuestDict[MasterQuestDict[questID].RequiredMissionID].State)
                    {
                        case QuestStates.Done:
                            Debug.Log("required mission done - MISSION CAN BE ADDED!!!");

                            if (MasterQuestDict[MasterQuestDict[questID].NextMissionID].State == QuestStates.Available)
                            {
                                MasterQuestDict[questID].State = QuestStates.Available;
                                npc.QuestDialogState = QuestDialogStates.HasQuest;
                                Debug.Log(this.GetType().FullName + " : " + "GF_Quest available id " + questID);
                            }

                            break;

                        case QuestStates.Available:
                            Debug.Log("required mission is available");
                            continue;
                        case QuestStates.Unavailable:
                            Debug.Log("required mission is unavailable");
                            continue;
                        case QuestStates.Completed:
                            Debug.Log("required mission is complete - go to missionGiver");
                            continue;
                        case QuestStates.Accepted:
                            Debug.Log("required mission is accepted - proceed mission");
                            continue;
                    }
                }
                else // mission hat keinen vorgaenger
                {
                    //  mission hat eine folgemission?
                    if (MasterQuestDict[questID].NextMissionID != 0)
                    {
                        MasterQuestDict[questID].State = QuestStates.Available;
                        Debug.Log(this.GetType().FullName + " : " + "GF_Quest available, beginns with id: " +
                                  questID.ToString() + " and continous with id: " +
                                  MasterQuestDict[questID].NextMissionID.ToString());
                    }
                    else
                    {
                        MasterQuestDict[questID].State = QuestStates.Available;
                        Debug.Log(this.GetType().FullName + " : " + "GF_Quest available & beginns with id: " +
                                  questID.ToString());
                    }
                }
            }

            // ##############################################
            //

            switch (npc.QuestDialogState)
            {
                case QuestDialogStates.HasQuest:
                    Debug.LogWarning("NPC has missions");
                    if (npc.QuestGreetingMessages.Count > 0)
                    {
                        npc.MissionGreetingMessage = npc.QuestGreetingMessages[0];
                    }
                    if (NpcQuestsToDisplay.Count > 0)
                    {
                        foreach (int _i in NpcQuestsToDisplay)
                        {
                            MasterQuestDict[_i].State = QuestStates.Available;
                            GF_GameManager.QuestLogInterface.AddQuestToDisplayedQuest(MasterQuestDict[_i]);
                        }
                    }
                    break;

                case QuestDialogStates.HasNoQuest:
                    Debug.LogWarning("NPC has no missions");
                    if (npc.QuestGreetingMessages.Count > 0)
                    {
                        npc.MissionGreetingMessage =
                            npc.QuestGreetingMessages[UnityEngine.Random.Range(0, npc.QuestGreetingMessages.Count)];
                    }
                    if (NpcQuestsToDisplay.Count > 0)
                    {
                        foreach (int _i in NpcQuestsToDisplay)
                        {
                            GF_GameManager.QuestLogInterface.AddQuestToDisplayedQuest(MasterQuestDict[_i]);
                        }
                    }
                    break;
            }
        }

        #endregion CheckQuestGiverForNextQuest

        #region AddExp

        private void AddExp(GF_Unit unit, int questLevel, int questXp)
        {
            Debug.Log("AddExp");
            GF_Exp.AddExperiencePoints(unit,
                                                 Get_Quest_Exp(GF_GameManager.Player.UnitAttribute.Level, 1, 100));
        }

        public int Get_Quest_Exp(int unitLevel, int questLevel, int questXp)
        {
            //Character_Level <= Quest_Level +  5 : Quest_XP = (100 %) or Full_Quest_XP
            //Character_Level  = Quest_Level +  6 : Quest_XP = ( 80 %) or ROUND(Full_Quest_XP * 0.8 / 5) * 5
            //Character_Level  = Quest_Level +  7 : Quest_XP = ( 60 %) or ROUND(Full_Quest_XP * 0.6 / 5) * 5
            //Character_Level  = Quest_Level +  8 : Quest_XP = ( 40 %) or ROUND(Full_Quest_XP * 0.4 / 5) * 5
            //Character_Level  = Quest_Level +  9 : Quest_XP = ( 20 %) or ROUND(Full_Quest_XP * 0.2 / 5) * 5
            //Character_Level >= Quest_Level + 10 : Quest_XP = ( 10 %) or ROUND(Full_Quest_XP * 0.1 / 5) * 5
            if (unitLevel <= questLevel + 3)
            {
                return questXp;
            }
            else if (unitLevel == questLevel + 4)
            {
                return Mathf.RoundToInt(questXp*0.8f);
            }
            else if (unitLevel == questLevel + 5)
            {
                return Mathf.RoundToInt(questXp*0.6f);
            }
            else if (unitLevel == questLevel + 6)
            {
                return Mathf.RoundToInt(questXp*0.4f);
            }
            else if (unitLevel == questLevel + 7)
            {
                return Mathf.RoundToInt(questXp*0.2f);
            }
            else
            {
                //Money in copper = XP * 6
                //LootGenerator.DROP_MONEY(GF_GameManager.PLAYER, Mathf.RoundToInt(_missionXp * 6));
                return 0;
            }
        }

        #endregion AddExp
    }


}

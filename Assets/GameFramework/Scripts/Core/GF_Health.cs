namespace GameFramework
{
    public class GF_Health
    {
        /// <summary>
        /// 
        /// </summary>
        /// <param name="unit"></param>
        /// <param name="hp"></param>
        /// <returns></returns>
        public static int AddHealthpoints(GF_Unit unit, int hp)
        {
            var h = unit.UnitAttribute.Health + hp;
            if (h > unit.UnitAttribute.MaxHealth)
                h = unit.UnitAttribute.MaxHealth;
            else if (h < 0)
                h = 0;
            return h;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="unit"></param>
        /// <param name="hp"></param>
        /// <returns></returns>
        public static int SubHealthpoints(GF_Unit unit, int hp)
        {
            var h = unit.UnitAttribute.Health - hp;
            if (h < 0)
            {
                h = 0;
                unit.UnitAttribute.IsAlive = false;
            }
            return h;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="attacker"></param>
        /// <param name="defender"></param>
        /// <param name="hp"></param>
        /// <returns></returns>
        public static int SubHealthpoints(GF_Unit attacker, GF_Unit defender, int hp)
        {
            var h = defender.UnitAttribute.Health - hp;
            if (h < 0)
            {
                h = 0;
                if (defender.UnitAttribute.IsAlive)
                    defender.OnDead(attacker);
                
                defender.UnitAttribute.IsAlive = false;
            }
            return h;
        }
    }

}
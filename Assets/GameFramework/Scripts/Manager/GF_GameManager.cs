using GameFramework;
using UnityEngine;

public class GF_GameManager : MonoBehaviour
{

    public static GF_Player Player { get; private set; }
    public static GF_Scoreboard Scoreboard { get; private set; }

    public GameOption GameOption = GameOption.Option1;
    public GamePlayMode GamePlayMode = GamePlayMode.SinglePlayer;
    public TeamSize TeamSize = TeamSize.TeamSize1;
    public VersusModes VersusMode = VersusModes.Pve;
    public GameDifficultyLevel DifficultyLevel = GameDifficultyLevel.Normal;

    public static void SetPlayer(GF_Player player)
    {
        Player = player;
        Debug.Log(player.UnitAttribute.Name);
    }

    public virtual void Awake()
    {
        Scoreboard = new GF_Scoreboard();
    }

    internal class QuestLogInterface
    {
        internal static void AddQuestToDisplayedQuest(GF_Quest gfQuest)
        {
            throw new System.NotImplementedException();
        }
    }
}

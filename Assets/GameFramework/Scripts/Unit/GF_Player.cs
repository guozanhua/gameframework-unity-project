using GameFramework;
using UnityEngine;
using System.Collections;


//[RequireComponent(typeof(GF_ThirdPersonController))]

public class GF_Player : GF_Unit
{

    // Use this for initialization
    public override void Start()
    {
        GF_GameManager.SetPlayer(this);

        if (target)
            GF_Exp.AddExperiencePoints(this, target);
        if (audioTest)
            GF_AudioManager.Play(audioTest, transform.position);
       
        SetHealth(UnitAttribute.MaxHealth);
        //UnitAttribute.Start();
        MyTransform = transform;
    }
     
    // Update is called once per frame
    public override void Update()
    {
       // UnitAttribute.Update();
    }

    public override void OnGUI()
    {
        //UnitAttribute.OnGUI();
    }
}

using System.Collections.Generic;
using GameFramework;
using UnityEngine;

public interface IBuilding
{
    void UnitFactory(GF_Unit unit);
}

[RequireComponent(typeof(GF_AnimationManager))]
public class GF_Unit : MonoBehaviour
{
    public CharacterController controller;
    public Transform MyTransform;
    public GF_Unit target;
    public UnitAttribute UnitAttribute;
    public List<AmmoMagazine> Ammo = new List<AmmoMagazine>();

    public AudioClip audioTest;

    [HideInInspector]
    public Vector3 moveDirection = Vector3.zero;

    #region public Methodes

    /// <summary>
    /// 
    /// </summary>
    /// <param name="health"></param>
    public void SetHealth(int health)
    {
        UnitAttribute.Health = health;
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="health"></param>
    public void SetMaxHealth(int health)
    {
        UnitAttribute.MaxHealth = health;
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="health"></param>
    public void AddHealth(int health)
    {
        UnitAttribute.Health = GF_Health.AddHealthpoints(this, health);
        if (UnitAttribute.Health <= 0)
        {
            Destroy(gameObject, 1);
        }
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="attacker"></param>
    /// <param name="health"></param>
    public void SubHealth(GF_Unit attacker, int health)
    {
        UnitAttribute.Health = GF_Health.SubHealthpoints(attacker, this, health);
        if (UnitAttribute.Health <= 0)
        {
            Destroy(gameObject, 1);
        }
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="health"></param>
    public void SubHealth(int health)
    {
        UnitAttribute.Health = GF_Health.SubHealthpoints(this, health);
        if (UnitAttribute.Health <= 0)
        {
            Destroy(gameObject, 1);
        }
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="team"></param>
    public void SetTeam(Team team)
    {
        UnitAttribute.Team = team;
    }

    #endregion public Methodes

    // Use this for initialization
    public virtual void Start()

    {
       // UnitAttribute.Start();
    }

    // Update is called once per frame
    public virtual void Update()
    {
        //UnitAttribute.Update();
    }

    public virtual void OnGUI()
    {
       // UnitAttribute.OnGUI();
    }

    /// <summary>
    /// 
    /// </summary>
    public virtual void Movement()
    {
        if (controller.isGrounded)
        {
            if (UnitAttribute.CanMove)
            {
                moveDirection = new Vector3(0, 0, Input.GetAxis("Vertical"));
                transform.Rotate(0, Input.GetAxis("Horizontal"), 0);

                if (UnitAttribute.CanJump && Input.GetButton("Jump"))
                    moveDirection.y = UnitAttribute.JumpSpeed;
            }
            else
            {
                moveDirection = Vector3.zero;
            }
            moveDirection = transform.TransformDirection(moveDirection);
            moveDirection *= UnitAttribute.Speed;
        }
        moveDirection.y -= UnitAttribute.GravityMultiplier * GF_Gamecore.Gravity * Time.deltaTime;
        controller.Move(moveDirection * Time.deltaTime);
    }

    /// <summary>
    /// 
    /// </summary>
    public virtual void Shot()
    {
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="attacker"></param>
    public void OnDead(GF_Unit attacker)
    {
        GF_Exp.AddExperiencePoints(attacker, this);
    }
}
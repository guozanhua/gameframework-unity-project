using System.Collections.Generic;
using UnityEngine;
using System.Collections;

public class GF_NPC : GF_Unit 
{

	// Use this for initialization
    public override void Start() 
    {
        SetHealth(UnitAttribute.MaxHealth);
	}
	
	// Update is called once per frame
    public override void Update()
    {
	
	}

    public override void Movement()
    {

    }

    public IEnumerable<int> NpcQuestIds
    {
        get { throw new System.NotImplementedException(); }
        set { throw new System.NotImplementedException(); }
    }

    public QuestDialogStates QuestDialogState
    {
        get { throw new System.NotImplementedException(); }
        set { throw new System.NotImplementedException(); }
    }

    public List<string> QuestGreetingMessages
    {
        get { throw new System.NotImplementedException(); }
        set { throw new System.NotImplementedException(); }
    }

    public string MissionGreetingMessage
    {
        get { throw new System.NotImplementedException(); }
        set { throw new System.NotImplementedException(); }
    }
}
